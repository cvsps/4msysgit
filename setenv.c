/*

  Implement setenv and friends for MinGW

UNIX man
    http://www.hmug.org/man/3/setenv.php
MSDN SetEnvironmentVariable
    http://msdn2.microsoft.com/en-us/library/ms686206.aspx

    char *
    getenv(const char *name);

    int
    setenv(const char *name, const char *value, int overwrite);

    int
    putenv(const char *string);

    void
    unsetenv(const char *name);
*/

#include <windows.h>

/*char * getenv(const char *name);*/
int setenv(const char *name, const char *value, int overwrite);
/*int putenv(const char *string);*/
void unsetenv(const char *name);

/* Implementation */

/* inserts or resets the environment variable name in the current environment 
list.  

If the variable name does not exist in the list, it is inserted with the 
given value.  

If the variable does exist, the argument overwrite is tested; 
if overwrite is zero, the variable is not reset, 
otherwise it is reset to the given value.

return the value 0 if successful;
     otherwise the value -1 is returned and the global variable errno is set
     to indicate the error.
*/
int setenv(const char *name, const char *value, int overwrite) {
  /* bug: ALWAYS OVERWRITE */
  return SetEnvironmentVariable(name, value) != 0 ? 0 /* OK */ : -1 /* FAIL */;
}

void unsetenv(const char *name) {
  SetEnvironmentVariable(name, NULL);
}
