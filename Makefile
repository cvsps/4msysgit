MAJOR=2
MINOR=1
CC=gcc
CFLAGS=-g -O2 -Wall 
CFLAGS+=-I. -I/git/compat -DVERSION=\"$(MAJOR).$(MINOR)\"
prefix?=/usr/local
OBJS=\
	tsearch/tsearch.o\
	tsearch/twalk.o\
	tsearch/tdelete.o\
	tsearch/tfind.o\
	strsep.o\
	setenv.o\
	cbtcommon/debug.o\
	cbtcommon/hash.o\
	cbtcommon/text_util.o\
	cbtcommon/sio.o\
	cbtcommon/tcpsocket.o\
	cvsps.o\
	cache.o\
	util.o\
	stats.o\
	cap.o\
	cvs_direct.o\
	list_sort.o

#.c.o:
#	$(CC) $< -o $@
	
all: cvsps

cvsps: $(OBJS)
	$(CC) -o cvsps $(OBJS) -lz /git/compat/regex.c -lwsock32

install:
	[ -d $(prefix)/bin ] || mkdir -p $(prefix)/bin
	[ -d $(prefix)/share/man/man1 ] || mkdir -p $(prefix)/share/man/man1
	install cvsps $(prefix)/bin
	install -m 644 cvsps.1 $(prefix)/share/man/man1

clean:
	rm -f cvsps *.o tsearch/*.o cbtcommon/*.o core

.PHONY: install clean
